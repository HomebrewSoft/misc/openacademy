# -*- coding: utf-8 -*-
{
    'name': 'Open Academy',
    'version': '13.0.0.4.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
    ],
    'data': [
        # security
        'security/groups.xml',
        'security/ir.model.access.csv',
        'security/openacademy_course.xml',
        # data
        'data/res_partner_category.xml',
        # demo
        'demo/openacademy_course.xml',
        # reports
        'reports/openacademy_course.xml',
        # views
        'views/menus.xml',
        'views/openacademy_course.xml',
        'views/openacademy_session.xml',
        'views/openacademy_wizard.xml',
        'views/res_partner_category.xml',
        'views/res_partner.xml',
    ],
}
